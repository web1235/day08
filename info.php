

<!DOCTYPE html>
<html lang='vn'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="style2.css"> 
</head>
<title>Info</title>

<body>
<?php


$falcutyArr = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');

session_start();
    $faculty_name = '';
    $key = '';

    if(isset($_SESSION["faculty_name"])&&isset($_SESSION["key"])){
        $faculty_name = $_SESSION["faculty_name"];
        $key = $_SESSION["key"];
    }

    if(isset($_POST['search'])){
        if(isset($_POST['faculty_name'])&&isset($_POST['key'])) {
            $faculty_name = $_POST['faculty_name'];
            $key = $_POST['key'];
            $_SESSION["faculty_name"] = $_POST['faculty_name'];
            $_SESSION["key"] = $_POST['key'];
        }
    } elseif (isset($_POST['delete'])){
        $faculty_name = '';
        $key = '';
        $_SESSION["faculty_name"] = '';
        $_SESSION["key"] = '';
    }
    ?>


<form name="infoForm" method="POST" enctype="multipart/form-data" action="">

    <div class='container'>
        <?php if (!empty($valid)) : ?>
            <div class="error">
                <?php foreach ($valid as $error) : ?>
                    <div><?php echo $error ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="info-col">
            <label  class="h-100">Khoa</label>
            <select class="h-100" name="faculty_name" >
            <?php
                                if($faculty_name==''){
                                    $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                                    foreach($facultyArr as $x=>$x_value){
                                        echo"<option>".$x_value."</option>";
                                    }
                                }else{
                                    echo "<option>".$faculty_name."</option>";
                                }
                            ?>

                
            </select>
        </div>
        <div class="info-col">
            <label class="h-100" >Từ khóa</label>
            <td height='40px'>
                <input type='text' class="h-100" name="key" value="<?php echo $key; ?>">
            </td>
        </div>
        <tr>
                <td>
                <button id='search' name = 'search' style='background-color: #7794ce; border-radius: 10px; 
                    width: 25%; height: 43px; border: 1.5px solid; margin: 15px 180px; color: white;'>Tìm kiếm</button>                                        
                <button id='delete' name = 'delete' style='background-color: #7e9ad2; border-radius: 10px; 
                    width: 80px; height: 43px; border: 1.5px solid; margin: 5px 0px; color: white;'>Xóa</button>
                </td>
            </tr>  


        </form>


    

        <div class="row">
            <div class="student">
                <p><b>Số sinh viên tìm thấy: </b></p>
            </div>
            <a href="signup.php" class="btn-submit">Thêm</a>
        </div>

        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Hoang Thi Ngoc Anh</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Do Anh Tuyet</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>Hoang Van B</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>